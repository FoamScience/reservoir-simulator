<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="compound.xsd" version="1.8.11">
  <compounddef id="a00005" kind="file" language="Markdown">
    <compoundname>mainpage.md</compoundname>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
    <programlisting>
<codeline><highlight class="normal">\mainpage<sp/>A<sp/>library<sp/>for<sp/>HPC<sp/>reservoir<sp/>simulations</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">\section<sp/>intro_sec<sp/>Introduction</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">This<sp/>library<sp/>relies<sp/>mainly<sp/>on<sp/>the<sp/>power<sp/>of<sp/>existing<sp/>OpenFOAM</highlight></codeline>
<codeline><highlight class="normal">functionality<sp/>to<sp/>build<sp/>highly<sp/>capable<sp/>solvers<sp/>for<sp/>(isotermic<sp/>at<sp/></highlight></codeline>
<codeline><highlight class="normal">the<sp/>moment)<sp/>fluid<sp/>flow<sp/>in<sp/>oil<sp/>and<sp/>gas<sp/>reservoirs.</highlight></codeline>
<codeline><highlight class="normal">It<sp/>should<sp/>compile<sp/>seamlessly<sp/>on<sp/>most<sp/>OpenFOAM<sp/>forks<sp/>with<sp/>versions</highlight></codeline>
<codeline><highlight class="normal">4.0+<sp/>although<sp/>the<sp/>native<sp/>dynamic<sp/>mesh<sp/>capabilities<sp/>of<sp/>OF6<sp/>solvers</highlight></codeline>
<codeline><highlight class="normal">are<sp/>not<sp/>used<sp/>(But<sp/>planned,<sp/>see</highlight></codeline>
<codeline><highlight class="normal">\ref<sp/>Foam.wells.verticalWell.updateCells</highlight></codeline>
<codeline><highlight class="normal">member<sp/>function).</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">\note</highlight></codeline>
<codeline><highlight class="normal">The<sp/>main<sp/>technical<sp/>focus<sp/>goes<sp/>to<sp/>(In<sp/>This<sp/>Order):</highlight></codeline>
<codeline><highlight class="normal"><sp/>-<sp/>Parallel<sp/>Execution<sp/>Efficiency,<sp/>both<sp/>on<sp/>POSIX<sp/>systems<sp/>and<sp/>Windows.</highlight></codeline>
<codeline><highlight class="normal"><sp/>-<sp/>Windows-friendly<sp/>Implementations<sp/>as<sp/>it&apos;s<sp/>probable<sp/>someone</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/>will<sp/>eventually<sp/>try<sp/>to<sp/>run<sp/>this<sp/>on<sp/>a<sp/>Windows<sp/>machine.</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">By<sp/>design,<sp/>the<sp/>library<sp/>doesn&apos;t<sp/>care<sp/>so<sp/>much<sp/>for<sp/>user<sp/>convenience.</highlight></codeline>
<codeline><highlight class="normal">Integration<sp/>with<sp/>some<sp/>GUI<sp/>packages<sp/>(<sp/>Think:<sp/></highlight></codeline>
<codeline><highlight class="normal">[Sim-Flow](https://sim-flow.com)<sp/>)<sp/>may<sp/>be<sp/>of<sp/></highlight></codeline>
<codeline><highlight class="normal">interest<sp/>at<sp/>a<sp/>later<sp/>point.<sp/>Users<sp/>are<sp/>expected<sp/>to<sp/>plot<sp/>things<sp/>using</highlight></codeline>
<codeline><highlight class="normal">*gnuplot*<sp/>or<sp/>*matplotlib*<sp/>until<sp/>a<sp/>capable<sp/>framework<sp/>of<sp/></highlight></codeline>
<codeline><highlight class="normal">**functionObjects**<sp/>is<sp/>developed.</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">\section<sp/>install_sec<sp/>Installation<sp/>and<sp/>Usage<sp/>Instructions</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">\subsection<sp/>step1<sp/>Compiling<sp/>the<sp/>library</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">To<sp/>compile<sp/>all<sp/>library<sp/>parts<sp/>to<sp/>binary<sp/>shared<sp/>objects<sp/>and</highlight></codeline>
<codeline><highlight class="normal">executables</highlight></codeline>
<codeline><highlight class="normal">(`Allwmake`<sp/>script<sp/>should<sp/>have<sp/>execution<sp/>permission):</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">\code{.bash}</highlight></codeline>
<codeline><highlight class="normal">./Allwmake</highlight></codeline>
<codeline><highlight class="normal">\endcode</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">Take<sp/>a<sp/>look<sp/>at<sp/>`Make/options`<sp/>for<sp/>each<sp/>library<sp/>department<sp/>to<sp/>change<sp/></highlight></codeline>
<codeline><highlight class="normal">compiled<sp/>classes<sp/>and<sp/>library<sp/>name,<sp/>...,<sp/>etc.</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">\subsection<sp/>step2<sp/>Use<sp/>in<sp/>own<sp/>solvers</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">One<sp/>can<sp/>dynamically<sp/>link<sp/>some<sp/>libraries<sp/>in<sp/>`controlDict`<sp/>of<sp/>a<sp/>simulation<sp/></highlight></codeline>
<codeline><highlight class="normal">case<sp/>by<sp/>adding<sp/>the<sp/>following<sp/>entry:</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">\code{.cpp}</highlight></codeline>
<codeline><highlight class="normal">libs</highlight></codeline>
<codeline><highlight class="normal">(</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>libporousIsoKFixedGradientPressure.so</highlight></codeline>
<codeline><highlight class="normal">);</highlight></codeline>
<codeline><highlight class="normal">\endcode</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">Which<sp/>should<sp/>allow<sp/>for<sp/>use<sp/>of<sp/>the<sp/>custom<sp/>pressure<sp/>boundary<sp/>condition</highlight></codeline>
<codeline><highlight class="normal">without<sp/>solver<sp/>modification.</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">If<sp/>a<sp/>newly-created<sp/>solver<sp/>is<sp/>to<sp/>instantiate<sp/>an<sp/>object<sp/>with<sp/>the<sp/>help</highlight></codeline>
<codeline><highlight class="normal">of<sp/>some<sp/>library<sp/>class,<sp/>it<sp/>must<sp/>be<sp/>statically<sp/>link<sp/>(At<sp/>compile<sp/>time).</highlight></codeline>
<codeline><highlight class="normal">\ref<sp/>impesFoam.C<sp/>is<sp/>the<sp/>simplest<sp/>example:</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">\code{.cpp}</highlight></codeline>
<codeline><highlight class="normal">//<sp/>Include<sp/>parent<sp/>class<sp/>header<sp/>file</highlight></codeline>
<codeline><highlight class="normal">#include<sp/>&quot;relativePermeabilityModel.H&quot;</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">//<sp/>Now<sp/>you<sp/>can<sp/>do<sp/>this<sp/>to<sp/>read<sp/>selected<sp/>permeability<sp/>model<sp/></highlight></codeline>
<codeline><highlight class="normal">//<sp/>And<sp/>its<sp/>parameters<sp/>from<sp/>constant/transportProperties</highlight></codeline>
<codeline><highlight class="normal">autoPtr&lt;relativePermeabilityModel&gt;<sp/>krModel<sp/>=<sp/>relativePermeabilityModel::New(&quot;krModel&quot;,<sp/>transportProperties,<sp/>Sw);</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">krModel-&gt;correct();<sp/>//<sp/>Most<sp/>models<sp/>have<sp/>a<sp/>correct<sp/>method<sp/>which<sp/>calculates<sp/>or<sp/>updates<sp/>model<sp/>results.</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">//<sp/>You<sp/>can<sp/>now<sp/>retrieve<sp/>values<sp/>of<sp/>interest<sp/>from<sp/>the<sp/>class</highlight></codeline>
<codeline><highlight class="normal">//<sp/>if<sp/>krw<sp/>is<sp/>an<sp/>initialized<sp/>volScalarField:</highlight></codeline>
<codeline><highlight class="normal">krw<sp/>=<sp/>krModel-&gt;krw();</highlight></codeline>
<codeline><highlight class="normal">//<sp/>would<sp/>return<sp/>the<sp/>krw<sp/>field<sp/>for<sp/>cells<sp/>in<sp/>the<sp/>current<sp/>processor<sp/>region.</highlight></codeline>
<codeline><highlight class="normal">\endcode</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">And<sp/>make<sp/>sure<sp/>the<sp/>solver&apos;s<sp/>`Make/options`<sp/>statically<sp/>links<sp/>the<sp/>library<sp/>in</highlight></codeline>
<codeline><highlight class="normal">question:</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">\code{.bash}</highlight></codeline>
<codeline><highlight class="normal">EXE_INC<sp/>=<sp/>\</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>#<sp/>Some<sp/>lnIncludes<sp/>the<sp/>solver<sp/>already<sp/>has</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>-I../../libs/permeabilityModels/lnInclude<sp/>#<sp/>&lt;--<sp/>path<sp/>to<sp/>library<sp/>sources</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">EXE_LIBS<sp/>=<sp/>\</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>#<sp/>Some<sp/>libraries<sp/>the<sp/>solver<sp/>already<sp/>loads</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>-L$(FOAM_USER_LIBBIN)<sp/>\<sp/>#<sp/>&lt;--<sp/>if<sp/>the<sp/>library<sp/>is<sp/>located<sp/>here<sp/>(it<sp/>should)</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>-lrelativePermeabilityModels<sp/><sp/>#<sp/>&lt;--<sp/>load<sp/>the<sp/>actual<sp/>library</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>#<sp/>This<sp/>assumes<sp/>the<sp/>shared<sp/>object<sp/>is<sp/>called<sp/>librelativePermeabilityModels.so</highlight></codeline>
<codeline><highlight class="normal">\endcode</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">\note</highlight></codeline>
<codeline><highlight class="normal">Changing<sp/>library<sp/>code<sp/>will<sp/>take<sp/>effect<sp/>immediately<sp/>(in<sp/>most<sp/>cases).<sp/>There</highlight></codeline>
<codeline><highlight class="normal">is<sp/>no<sp/>need<sp/>to<sp/>re-compile<sp/>solvers.</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">\subsection<sp/>step3<sp/>Compiling<sp/>native<sp/>Windows<sp/>executable</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">The<sp/>recommended<sp/>way<sp/>is<sp/>to<sp/>use<sp/>blueCFD&apos;s<sp/>code<sp/>(a<sp/>port<sp/>of<sp/>OpenFOAM<sp/>to<sp/>Windows).</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">Average<sp/>execution<sp/>time<sp/>for<sp/>solvers<sp/>is<sp/>best<sp/>inspected<sp/>using<sp/>Windows&apos;<sp/>Native<sp/>PowerShell:</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">\code{.sh}</highlight></codeline>
<codeline><highlight class="normal">for<sp/>i<sp/>in<sp/>{1..100};<sp/>do<sp/>powershell.exe<sp/>Measure-Command{impesFoam}<sp/>&gt;&gt;<sp/>log;<sp/>done</highlight></codeline>
<codeline><highlight class="normal">\endcode</highlight></codeline>
<codeline></codeline>
<codeline></codeline>
<codeline></codeline>
<codeline><highlight class="normal">\section<sp/>structure_sec<sp/>Structure<sp/>and<sp/>Current<sp/>Status</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">The<sp/>structure<sp/>of<sp/>the<sp/>framework<sp/>and<sp/>current<sp/>features<sp/>are<sp/>summaried<sp/>in<sp/>the<sp/>following</highlight></codeline>
<codeline><highlight class="normal">image:</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">\image<sp/>html<sp/><sp/>svg/mindmap.svg<sp/>&quot;Framework<sp/>structure&quot;</highlight></codeline>
<codeline><highlight class="normal">\image<sp/>latex<sp/>pdf/mindmap.pdf</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">\section<sp/>Library<sp/>and<sp/>Solver<sp/>Validation</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">The<sp/>current<sp/>benchmarks<sp/>should<sp/>be<sp/>used<sp/>to<sp/>verify<sp/>libraries:</highlight></codeline>
<codeline><highlight class="normal">-<sp/>Buckley-Levrette<sp/>Theory.</highlight></codeline>
<codeline><highlight class="normal">-<sp/>Inject<sp/>water<sp/>from<sp/>a<sp/>well,<sp/>produce<sp/>from<sp/>another.</highlight></codeline>
<codeline><highlight class="normal">-<sp/>Capillarity<sp/>check.</highlight></codeline>
<codeline><highlight class="normal"><sp/></highlight></codeline>
<codeline><highlight class="normal">For<sp/>the<sp/>solvers,<sp/>SPE<sp/>benchmarks<sp/>should<sp/>be<sp/>imployed.</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">The<sp/>following<sp/>solvers/utilities<sp/>are<sp/>extensively<sp/>verified:</highlight></codeline>
<codeline><highlight class="normal">-<sp/>egridToFoam.C</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">The<sp/>following<sp/>solvers<sp/>are<sp/>found<sp/>to<sp/>be<sp/>working<sp/>reasonably<sp/>well<sp/>on<sp/>some<sp/>cases:</highlight></codeline>
<codeline><highlight class="normal">-<sp/>imesFoam.C<sp/>(Injection-Production)</highlight></codeline>
    </programlisting>
    <location file="mainpage.md"/>
  </compounddef>
</doxygen>
