\mainpage A library for HPC reservoir simulations

\section intro_sec Introduction

This library relies mainly on the power of existing OpenFOAM
functionality to build highly capable solvers for (isotermic at 
the moment) fluid flow in oil and gas reservoirs.
It should compile seamlessly on most OpenFOAM forks with versions
4.0+ although the native dynamic mesh capabilities of OF6 solvers
are not used (But planned, see
\ref Foam.wells.verticalWell.updateCells
member function).

\note
The main technical focus goes to (In This Order):
 - Parallel Execution Efficiency, both on POSIX systems and Windows.
 - Windows-friendly Implementations as it's probable someone
   will eventually try to run this on a Windows machine.

By design, the library doesn't care so much for user convenience.
Integration with some GUI packages ( Think: 
[Sim-Flow](https://sim-flow.com) ) may be of 
interest at a later point. Users are expected to plot things using
*gnuplot* or *matplotlib* until a capable framework of 
**functionObjects** is developed.

\section install_sec Installation and Usage Instructions

\subsection step1 Compiling the library

To compile all library parts to binary shared objects and
executables
(`Allwmake` script should have execution permission):

\code{.bash}
./Allwmake
\endcode

Take a look at `Make/options` for each library department to change 
compiled classes and library name, ..., etc.

\subsection step2 Use in own solvers

One can dynamically link some libraries in `controlDict` of a simulation 
case by adding the following entry:

\code{.cpp}
libs
(
    libporousIsoKFixedGradientPressure.so
);
\endcode

Which should allow for use of the custom pressure boundary condition
without solver modification.

If a newly-created solver is to instantiate an object with the help
of some library class, it must be statically link (At compile time).
\ref impesFoam.C is the simplest example:

\code{.cpp}
// Include parent class header file
#include "relativePermeabilityModel.H"

// Now you can do this to read selected permeability model 
// And its parameters from constant/transportProperties
autoPtr<relativePermeabilityModel> krModel = relativePermeabilityModel::New("krModel", transportProperties, Sw);

krModel->correct(); // Most models have a correct method which calculates or updates model results.

// You can now retrieve values of interest from the class
// if krw is an initialized volScalarField:
krw = krModel->krw();
// would return the krw field for cells in the current processor region.
\endcode

And make sure the solver's `Make/options` statically links the library in
question:

\code{.bash}
EXE_INC = \
    # Some lnIncludes the solver already has
    -I../../libs/permeabilityModels/lnInclude # <-- path to library sources

EXE_LIBS = \
    # Some libraries the solver already loads
    -L$(FOAM_USER_LIBBIN) \ # <-- if the library is located here (it should)
    -lrelativePermeabilityModels  # <-- load the actual library
    # This assumes the shared object is called librelativePermeabilityModels.so
\endcode

\note
Changing library code will take effect immediately (in most cases). There
is no need to re-compile solvers.

\subsection step3 Compiling native Windows executable

The recommended way is to use blueCFD's code (a port of OpenFOAM to Windows).

Average execution time for solvers is best inspected using Windows' Native PowerShell:

\code{.sh}
for i in {1..100}; do powershell.exe Measure-Command{impesFoam} >> log; done
\endcode



\section structure_sec Structure and Current Status

The structure of the framework and current features are summaried in the following
image:

\image html  svg/mindmap.svg "Framework structure"
\image latex pdf/mindmap.pdf

\section Library and Solver Validation

The current benchmarks should be used to verify libraries:
- Buckley-Levrette Theory.
- Inject water from a well, produce from another.
- Capillarity check.
 
For the solvers, SPE benchmarks should be imployed.

The following solvers/utilities are extensively verified:
- egridToFoam.C

The following solvers are found to be working reasonably well on some cases:
- imesFoam.C (Injection-Production)
