#!/bin/bash - 
#===============================================================================
#
#          FILE: setUpField.sh
# 
#         USAGE: ./setUpField.sh input.csv output.dat
# 
#   DESCRIPTION:  One shell command to treat praview pointField data
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 04/07/2019 12:49
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
paste -d',' <(cut -d',' -f5-7 $1) <(cut -d',' -f2 $1) | column -t -s ',' > $2

