\mainpage ASCII EGRID to OpenFOAM mesh converter

\image html eclipseGrid.png "Special Image label"

\section intro_sec Introduction

A library to introduce common isotropic permeability 
models to OpenFOAM solvers: Mainly *Kozeny-Carman* and *Happel-Brenner*.

\note
Not intended for production use!! Created as a playground to 
learn how to program new OpenFOAM libraries.

\section install_sec Installation

\subsection step1 1.Compiling the library
To compile the library to a binary shared object 
(`libpermeabilityModels.so`):

\code{.sh}
wmake libso
\endcode

Take a look at `Make/options` to change compiled classes and library name ... etc.

\subsection step2 2.Use in own solver
Just include the base class header in your solver
\code{.cpp}
#include "permeabilityModel.H"
\endcode

And make sure the solver's `Make/options` properly links the library:

\code{.sh}
EXE_INC = \
    # Some lnIncludes you already have
    -I../../libs/permeabilityModels/lnInclude # <-- path to library includes

EXE_LIBS = \
    # Some libraries you already loaded 
    -L$(FOAM_USER_LIBBIN) \ # <-- if the library is located here (it should)
    -lpermeabilityModels  # <-- load the actual library
\endcode

\note
As you already now, changing library code takes effect immediately and there
is no need to recompile the solver if the solver is still good for the library.
