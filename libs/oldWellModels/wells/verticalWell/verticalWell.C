/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011-2016 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "verticalWell.H"
#include "addToRunTimeSelectionTable.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
namespace wells
{
defineTypeNameAndDebug(verticalWell, 0);

addToRunTimeSelectionTable
(
    well,
    verticalWell,
    dictionary
);
}
}

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::wells::verticalWell::verticalWell
(
    const word& name,
    const dictionary& wellsProperties,
    const fvMesh& p
)
:
    well(name, wellsProperties, p)
{
}

// * * * * * * * * * * * * * Private Member Functions * * * * * * * * * * * //

void Foam::wells::verticalWell::updateCells()
{
    // First, a global check for out-of-bounds wells

    // cell ID local to processors
    labelList localCellId(Pstream::nProcs(),0);
    // Each processor will log its entry here
    localCellId[Pstream::myProcNo()] = mesh_.findCell(position_);
    // Reduce list so each processor where the cell is created
    reduce(localCellId, maxOp<labelList>());

    // If no processor found a corresponding cell in its mesh section
    if (max(localCellId) <= -1)
    {
        FatalErrorIn("void Foam::wells::verticalWell::updateCells()")
            << "Well " << name_ <<  " at "
            << position_ << "  is out of mesh boundaries."
            << exit(FatalError);
    } else {
        // Store responsible processor
        proc_ = findIndex(localCellId, max(localCellId));
    }

    if (Pstream::nProcs()>1 && Pstream::myProcNo() == proc_)
        Pout << "Well " << name_ << " is located in processor "
             << Pstream::myProcNo() << " Region" << endl;

    cells_.setSize(1);
    cells_[0] = mesh_.findCell(position_);
}
// ************************************************************************* //
