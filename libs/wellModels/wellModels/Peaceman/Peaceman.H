/*---------------------------------------------------------------------------*\
=========                 |
\\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
 \\    /   O peration     |
  \\  /    A nd           | Copyright (C) 2011-2016 OpenFOAM Foundation
   \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::blackoilWellModels::Peaceman

Description
    A class implementing a "workaround" for removing singularity when 
    solving for wells BHP; Taking the most simple case. 

SourceFiles
    Peaceman.C

\*---------------------------------------------------------------------------*/

#ifndef Peaceman_H
#define Peaceman_H

#include "wellModel.H"
#include "uniformDimensionedFields.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
namespace blackoilWellModels
{

/*---------------------------------------------------------------------------*\
                  Class Peaceman Declaration
\*---------------------------------------------------------------------------*/

class Peaceman
:
    public wellModel
{

    // Private Data Members 

        //- Refs to relative permeabilities
        const volScalarField& kr1_;
        const volScalarField& kr2_;

        //- Ref to oil fractional flow;
        //  Should be calculated once in solver
        //  FIXME: We are interested in knowing Fw only in Well cells if TOTALRATE is
        //  specified, aren't we?
        //  Why calculate it everywhere then??
        //  DISCUSSION: Complications of making this abstract
        //  - Qw/(Qw+Qo) should use Qi = -(K kri A/mu)*grad(p)
        //   and if grad(p) is volScalarField we're good to go
        const volScalarField& waterFrac_;

        //- Gravity field
        uniformDimensionedVectorField g_;

public:

    //- Runtime type information
    TypeName("Peaceman");

    // Constructors

        //- Construct from components
        Peaceman
        (
            const word& name,
            const dictionary& wellsProperties,
            const fvMesh& mesh,
            const HCmixture<blackoilPhase>& mixture,
            const relativePermeabilityModel& krModel
        );

    //- Destructor
    ~Peaceman() {}

    // Member Functions
    
        //- Correct well model
        void correct();

        //- Calculate flowRate-operated well source
        void sourceRateOperated(well& thisWell);

        //- Correct coeffs for fixed-rate water injection
        void injectRate(well& thisWell, const word& phase, const labelList& cells);

        //- Correct coeffs for fixed-rate production
        void produceRate(well& thisWell, const labelList& cells);
};

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace wellModels

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
