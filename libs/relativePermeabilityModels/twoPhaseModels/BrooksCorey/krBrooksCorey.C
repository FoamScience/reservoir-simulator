/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011-2016 OpenFOAM Foundation 
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "krBrooksCorey.H"
#include "addToRunTimeSelectionTable.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
namespace relativePermeabilityModels
{
defineTypeNameAndDebug(krBrooksCorey, 0);

addToRunTimeSelectionTable
(
    relativePermeabilityModel,
    krBrooksCorey,
    dictionary
);
}
}

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::relativePermeabilityModels::krBrooksCorey::krBrooksCorey
(
    const word& name,
    const dictionary& transportProperties,
    const phase& phase1,
    const phase& phase2
)
    :
    relativePermeabilityModel(name, transportProperties, phase1, phase2),
    Smin_
    (
        IOobject
        (
            phase1_.name()+".alphaMin",
            S_.time().timeName()+"/"+typeName,
            S_.mesh(),
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        ),
        S_.mesh()
    ),
    Smax_
    (
        IOobject
        (
            phase1_.name()+".alphaMax",
            S_.time().timeName()+"/"+typeName,
            S_.mesh(),
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        ),
        S_.mesh()
    ),
    n_
    (
        IOobject
        (
            "n",
            S_.time().timeName()+"/"+typeName,
            S_.mesh(),
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        ),
        S_.mesh()
    ),
    Se_((S_-Smin_)/(Smax_-Smin_)),
    kr1Max_
    (
        IOobject
        (
            phase1_.name()+".krMax",
            S_.time().timeName()+"/"+typeName,
            S_.mesh(),
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        ),
        S_.mesh()
    ),
    kr2Max_
    (
        IOobject
        (
            phase2_.name()+".krMax",
            S_.time().timeName()+"/"+typeName,
            S_.mesh(),
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        ),
        S_.mesh()
    )
{

    // Input ckecks
    if (gMin(n_) <= 0)
    {
        FatalErrorIn(
            "Foam::relativePermeabilityModels::krBrooksCorey::krBrooksCorey\
            (\
                const word& name,\
                const dictionary& transportProperties,\
                const phase& phase1,\
                const phase& phase2\
            ) ")
            << "Relative permeability coefficient n equal or less than 0" 
            << exit(FatalError);
    }

    if (gMin(Smin_) <= 0 || gMax(Smin_) >= 1 )
    {
        FatalErrorIn(
            "Foam::relativePermeabilityModels::krBrooksCorey::krBrooksCorey\
            (\
                const word& name,\
                const dictionary& transportProperties,\
                const phase& phase1,\
                const phase& phase2\
            ) ")
            << "Unexpected saturation " << Smin_.name() << "value" 
            << exit(FatalError);
    }

    if (gMin(Smax_) <= 0 || gMax(Smax_) >= 1 )
    {
        FatalErrorIn(
            "Foam::relativePermeabilityModels::krBrooksCorey::krBrooksCorey\
            (\
                const word& name,\
                const dictionary& transportProperties,\
                const phase& phase1,\
                const phase& phase2\
            ) ")
            << "Unexpected saturation " << Smax_.name() << "value" 
            << exit(FatalError);
    }
}

// * * * * * * * * * * * Public Member Functions * * * * * * * * * * * * * * //

void Foam::relativePermeabilityModels::krBrooksCorey::correct()
{
    if (debug)
    {
        forAll(S_.internalField(), celli)
        {
            if (S_[celli] < Smin_[celli])
                WarningIn
                (
                    "void Foam::relativePermeabilityModels::krBrooksCorey::correct()"
                )
                << "Underflow, " << phase1_.name() 
                << " saturation falls bellow minimal saturation at cell "
                << celli << endl;
            if (S_[celli] > Smax_[celli])
                WarningIn
                (
                    "void Foam::relativePermeabilityModels::krBrooksCorey::correct()"
                )
                << "Overflow, " << phase1_.name() 
                << " saturation exceeds maximal saturation at cell "
                << celli << endl;
        }
    }

    // Calculate with BC overwriting
    // Otherwise use correctBoundaryConditions()
    Se_  == (S_-Smin_)/(Smax_-Smin_);
    Se_.correctBoundaryConditions();

    kr1_ = kr1Max_ * pow(Se_,n_);
    kr2_ = kr2Max_ * pow((1.0-Se_),n_);
    dkr1dS_ =  kr1Max_*n_*pow(Se_,n_-1.0)/(Smax_-Smin_);	
    dkr2dS_ = -kr2Max_*n_*pow((1.0-Se_),n_-1.0)/(Smax_-Smin_);

    kr1_.correctBoundaryConditions();
    kr2_.correctBoundaryConditions();
    dkr1dS_.correctBoundaryConditions();
    dkr2dS_.correctBoundaryConditions();


}

// ************************************************************************* //
