/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011-2016 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Namespace
    Foam::phases

Description
    A namespace to hold different kinds of phase objects. 

Class
    Foam::phase

Description
    An abstract base class for fluid phases used in reservoir simulations.

SourceFiles
    phase.C

\*---------------------------------------------------------------------------*/

#ifndef phase_H
#define phase_H

#include "volFields.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                  Class phase Declaration
\*---------------------------------------------------------------------------*/

class phase
{

protected:

    // Protected data
    
        //- Phase Name
        word name_;

        //- Phase dictionary entry in transport dict
        dictionary phaseDict_;

        //- Const access to the mesh
        const fvMesh& mesh_;

        //- Phase viscosity Field
        volScalarField nu_;

        //- Phase velocity field
        volVectorField U_;

public:

    //- Runtime type information
    TypeName("phase");


    // Declare run-time constructor selection table
    declareRunTimeSelectionTable
    (
        autoPtr,
        phase,
        dictionary,
        (
            const word& name,
            const dictionary& phaseDict,
            const fvMesh& mesh
        ),
        (name, phaseDict, mesh)
    );


    // Selectors

        //- Return a reference to the selected permeability model
        static autoPtr<phase> New
        (
            const word& name,
            const dictionary& phaseDict,
            const fvMesh& mesh
        );


    // Constructors

        //- Construct from components
        phase
        (
            const word& name,
            const dictionary& phaseDict,
            const fvMesh& mesh
        );


    //- Destructor
    virtual ~phase() {}


    // Member Functions

        //- Return Phase name
        const word& name() const {
            return name_;
        }

        //- Return mesh ref
        const fvMesh& mesh() const {
            return mesh_;
        }

        //- Return ref to velocity field
        volVectorField& U() {
            return U_;
        }

        //- Calculate viscosity
        virtual void calcNu() = 0;
};

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
