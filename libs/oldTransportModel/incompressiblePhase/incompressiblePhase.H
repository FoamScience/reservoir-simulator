/*---------------------------------------------------------------------------*\
=========                 |
\\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
 \\    /   O peration     |
  \\  /    A nd           | Copyright (C) 2011-2016 OpenFOAM Foundation
   \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::phases::incompressiblePhase

Description
    Implements an incompressible fluid phase.

SourceFiles
    incompressiblePhase.C

\*---------------------------------------------------------------------------*/

#ifndef incompressiblePhase_H
#define incompressiblePhase_H

#include "phase.H"
#include "viscosityModel.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
namespace phases
{

/*---------------------------------------------------------------------------*\
                  Class incompressiblePhase Declaration
\*---------------------------------------------------------------------------*/

class incompressiblePhase
:
    public phase
{

protected:
    
    // Protected Data Memebers
        
        //- Phase density
        dimensionedScalar rho_;

        //- Phase flux
        autoPtr<surfaceScalarField> PtrPhi_;

        //- Viscosity model
        autoPtr<viscosityModel> nuModel_;

        //- Dynamic viscosity
        volScalarField mu_;

public:

    //- Runtime type information
    TypeName("incompressiblePhase");

    // Constructors

    //- Construct from components
    incompressiblePhase
    (
        const word& name,
        const dictionary& phaseDict,
        const fvMesh& mesh
    );

    //- Destructor
    ~incompressiblePhase() {}

    // Member Functions
    
        //- Calculate relative permeability
        void calcNu() {
            nuModel_->correct();
        }

        //- Return ref to phase flux
        surfaceScalarField& phi() const {
            return PtrPhi_();
        }

        //- Return dynamic viscosity
        const tmp<volScalarField> mu() const {
            return mu_;
        }

        //- Return phase density
        const dimensionedScalar& rho() const {
            return rho_;
        }
};

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace phases

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
