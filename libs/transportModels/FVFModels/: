/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
  \\    /   O peration     |
  \\  /    A nd           | Copyright (C) 2011-2016 OpenFOAM Foundation
  \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::FVFModels::polynomial

Description
    Standard two-phases Brooks-Corey relative permeability model:
    \f[
        k_{rw} = k_{rw, max}S_{w, norm}^n
    \f]
    \f[
        k_{rn} = k_{rn, max}(1-S_{w, norm})^n
    \f]

    Where 
    \f[
        S_{w, norm} = \frac{S_w, S_{w, irr}}{1-S_{w, irr}-S_{n, irr}}
    \f]

    \vartable
        k_r             | Relative permeability
        w,n             | Wetting and NonWetting indices respectively
        S_{w, norm}     | Normalized wetting-phase saturation
        S_{w, irr}      | Irreducible wetting phase saturation (`Swmin` in `phaseDict`)
        S_{n, irr}      | Irreducible wetting phase saturation (`Swmax = 1-Sn_irr` in `phaseDict`)
    \endtable

SourceFiles
    polynomial.C

\*---------------------------------------------------------------------------*/

#ifndef polynomial_H
#define polynomial_H

#include "FVFModel.H"
#include "UList.H"
#include "Function1.H"
#include "polynomialFunction.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
namespace FVFModels
{

/*---------------------------------------------------------------------------*\
    Class polynomial Declaration
\*---------------------------------------------------------------------------*/

class polynomial
:
    public FVFModel
{
    //- Private Data Members
        
        //- Model coeffs
        UList<scalar> polynomialCoeffs_;

        //- polynomialFunction
        polynomialFunction polyFunc_;
        
        //- polynomialFunction derivative
        polynomialFunction dpolyFuncdx_;

        
public:

    //- Runtime type information
    TypeName("polynomial");

    // Constructors

    //- Construct from components
    polynomial
    (
        const word& name,
        const dictionary& phaseDict,
        const fvMesh& mesh
    );

    //- Destructor
    ~polynomial()
        {}

    // Member Functions

    //- Correct the FVF
    void correct()
        {
            // Calculate FVF at cell centers
            forAll(mesh_.cells(),celli) {
                diFVFdP_.internalField()[celli] = pow(1/p_[celli],2)*dpolyFuncdx_(p_[celli]);
            }

            // Correct boundary conditions
            diFVFdP_.correctBoundaryConditions();
        }

};

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace FVFModels

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
