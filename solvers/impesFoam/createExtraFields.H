// Convenience
dimensionedScalar VSMALLF ("VSMALLF", dimensionSet(-1,3,1,0,0,0,0), VSMALL);


// Relative permeability
krModel->correct();
volScalarField krn = krModel->phase2Kr();
volScalarField krw = krModel->phase1Kr();
volScalarField dkrndS = krModel->phase2dKrdS();
volScalarField dkrwdS = krModel->phase1dKrdS();

// Interpolation to faces
surfaceScalarField krnf ("krnf",fvc::interpolate(krn,"krn"));
surfaceScalarField krwf ("krwf",fvc::interpolate(krw,"krw"));
surfaceScalarField dkrnfdS ("dkrnfdS",fvc::interpolate(dkrndS,"krn"));
surfaceScalarField dkrwfdS ("dkrwfdS",fvc::interpolate(dkrwdS,"krw"));
surfaceScalarField munf ("munf",fvc::interpolate(mun,"mu"));
surfaceScalarField muwf ("muwf",fvc::interpolate(muw,"mu"));
surfaceScalarField rhonf ("rhonf",fvc::interpolate(rhon,"rho"));
surfaceScalarField rhowf ("rhowf",fvc::interpolate(rhow,"rho"));

// Mobilities and flow fractions
surfaceScalarField Mnf ("Mnf",Kf*krnf/munf);
surfaceScalarField Lnf ("Lnf",rhonf*Kf*krnf/munf);	
surfaceScalarField Mwf ("Mwf",Kf*krwf/muwf);
surfaceScalarField Lwf ("Lwf",rhowf*Kf*krwf/muwf);
surfaceScalarField Mf ("Mf",Mnf+Mwf);
surfaceScalarField Lf ("Lf",Lnf+Lwf);
surfaceScalarField Fwf ("Fbf",Mwf/(Mf+VSMALLF));
volScalarField Fw ("Fw",(krw/muw) / ( (krn/mun) + (krw/muw) ));

// Capillary Model
pcModel->correct();

// Capillary flux
surfaceScalarField phiPc("phiPc",Mwf * fvc::interpolate(pcModel->dpcdS(),"pc")* fvc::snGrad(phasew.alpha()) * mesh.magSf());

// Gravitational flux
surfaceScalarField phiG("phiG", (Lf * g) & mesh.Sf());

// Wells Dictionary
IOdictionary wellsProperties
(
    IOobject
    (
        "wellsProperties",
        runTime.constant(),
        mesh,
        IOobject::MUST_READ,
        IOobject::NO_WRITE
    )
);

// Wells model
autoPtr<wellModel> wModel = wellModel::New("wModel", wellsProperties, mesh, mixture, krModel);
wModel->correct();

// Well Sources & Sinks
volScalarField wSource = wModel->ExplicitWaterSource();
volScalarField oSource = wModel->ExplicitOilSource();

//// Well Sources & Sinks
//volScalarField SrcProd
//(
//    IOobject
//    (
//        "SrcProd",
//        runTime.timeName(),
//        mesh,
//        IOobject::NO_READ,
//        IOobject::AUTO_WRITE
//    ),
//    wModel->production()
//);
//volScalarField SrcInj
//(
//    IOobject
//    (
//        "SrcInj",
//        runTime.timeName(),
//        mesh,
//        IOobject::NO_READ,
//        IOobject::AUTO_WRITE
//    ),
//    wModel->injection()
//);
//
//SrcProd.write();
//SrcInj.write();
