// This should belong to an Impes algo. class
scalar dSmax(runTime.controlDict().lookupOrDefault<scalar>("dSmax",0.));

Info<< "Reading field p\n" << endl;
volScalarField p
(
    IOobject
    (
        "p",
        runTime.timeName(),
        mesh,
        IOobject::MUST_READ,
        IOobject::AUTO_WRITE
    ),
    mesh
);


Info<< "Reading transportProperties\n" << endl;
IOdictionary transportProperties
(
    IOobject
    (
        "transportProperties",
        runTime.constant(),
        mesh,
        IOobject::MUST_READ,
        IOobject::NO_WRITE
    )
);

dimensionedScalar Swmin(transportProperties.lookupOrDefault("phasew.alpha()min",dimensionedScalar("phasew.alpha()min",dimless,0)));

// Incompressible phases

autoPtr<HCmixture<blackoilPhase> > mixture = HCmixture<blackoilPhase>::New("twoPhase",transportProperties,mesh);
blackoilPhase& phasen = mixture->phase("nonWetting");
blackoilPhase& phasew = mixture->phase("wetting");

//// NonWetting phase 
volVectorField& Un = phasen.U();
surfaceScalarField& phin = phasen.phi();
volScalarField& rhon = phasen.rho();
volScalarField& mun = phasen.mu();
const dimensionedScalar& rhoScn = phasen.standardRho();

//// Wetting phase 
volVectorField& Uw = phasew.U();
surfaceScalarField& phiw = phasew.phi();
volScalarField& rhow = phasew.rho();
volScalarField& muw = phasew.mu();
const dimensionedScalar& rhoScw = phasew.standardRho();


// Viscosity ratio for convenience
volScalarField Mmu(muw/mun);

// Relative permeability model 
autoPtr<relativePermeabilityModel> krModel 
    = 
    relativePermeabilityModel::New
    (
            "krModel",
            transportProperties, 
            phasew,
            phasen
    );

//volScalarField p_rgh
//(
//    IOobject
//    (
//        "p_rgh",
//        runTime.timeName(),
//        mesh,
//        IOobject::NO_READ,
//        IOobject::AUTO_WRITE
//    ),
//    mesh,
//    p,
//    "zeroGradient"
//);
//forAll(mesh.C(), celli)
//{
//    p_rgh.internalField()[celli] = p[celli] - rhon[celli] * g[0].value() * (0.0-mesh.C()[celli].z());
//}
//p_rgh.correctBoundaryConditions();

// Porosity	
volScalarField porosity
(
    IOobject
    (
        "porosity",
        runTime.timeName(),
        mesh,
        IOobject::MUST_READ,
        IOobject::AUTO_WRITE
    ),
    mesh
);

// Compressibility
volScalarField compressibility
(
    IOobject
    (
        "compressibility",
        runTime.constant(),
        mesh,
        IOobject::MUST_READ,
        IOobject::NO_WRITE
    ),
    mesh
);

// Permeability       
Info<< "Reading field K\n" << endl;
volScalarField K
(
    IOobject
    (
        "K",
        runTime.timeName(),
        mesh,
        IOobject::MUST_READ,
        IOobject::AUTO_WRITE
    ),
    mesh
);


// Permeability interpolation to faces
surfaceScalarField Kf = fvc::interpolate(K,"K");

Info<< "Reading field U\n" << endl;
volVectorField U
(
    IOobject
    (
        "U",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::NO_WRITE
    ),
    Un + Uw
);

// Assume this is total flux
#include "createPhi.H"

// Pressure flux
surfaceScalarField phiP = phi;

// Capillary pressure
autoPtr<capillaryPressureModel> pcModel 
    = 
    capillaryPressureModel::New
    (
            "pcModel",
            transportProperties, 
            phasew,
            phasen
    );

// A fix for a known foam-extend bug 
mesh.schemesDict().setFluxRequired(p.name());

// Coupled working variable
volVector2Field Swp
(
    IOobject
    (
        "Swp",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::AUTO_WRITE
    ),
    mesh,
    dimensionedVector2("zero",dimless,vector2::zero)
);
