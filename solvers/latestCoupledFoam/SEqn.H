{

    //// Store old values and update BCs
    //Un.correctBoundaryConditions();
    //Uw.correctBoundaryConditions();
    //// Set refs for phi if U is a fixedValue
    //forAll(mesh.boundary(),patchi)
    //{
    //    if (isA< fixedValueFvPatchField<vector> >(Un.boundaryField()[patchi]))
    //    {
    //        phin.boundaryField()[patchi] = Un.boundaryField()[patchi] & mesh.Sf().boundaryField()[patchi];
    //    }
    //    if (isA< fixedValueFvPatchField<vector> >(Uw.boundaryField()[patchi]))
    //    {
    //        phiw.boundaryField()[patchi] = Uw.boundaryField()[patchi] & mesh.Sf().boundaryField()[patchi];
    //    }
    //}
  
    fvScalarMatrix SwEqn
        (
            fvm::ddt(wStorage, phasew.alpha()) 
            //Difference of phi

            + wSource
            // Explicit Wells contribution 
            //- SrcwInj*phasew.rFVF() + SrcwProd*phasew.rFVF()
        );

    //debugMatrix(SwEqn, "SwEqn");

    fvScalarMatrix pInSw
    (
        fvm::laplacian(-Mwf, p)
        + fvc::div(phiwG) 
        // Implicit Wells contribution
        //+ fvm::Sp( ImpwProd*phasew.rFVF()-ImpwInj*phasew.rFVF(), p)
    );

    //debugMatrix(pInSw, "pInSw");

    //// Populate block matrix
    //forAll(upper, nei)
    //{
    //    upper[nei][0] = SwEqn.upper()[nei];
    //    upper[nei][2] = pInSw.upper()[nei];
    //    lower[nei][0] = SwEqn.lower()[nei];
    //    lower[nei][2] = pInSw.lower()[nei];
    //}
    //forAll(diag, celli)
    //{
    //    diag[celli][0] = SwEqn.diag()[celli];
    //    source[celli][0] = SwEqn.source()[celli] + pInSw.source()[celli];
    //}
    
    //phasew.alpha().relax();

    resEqn.insertEquation(0, SwEqn);
    resEqn.insertEquationCoupling(0,1, pInSw);
    //resEqn.insertEquationCoupling(0,1, divPhiwGV);
}
