
void debugMatrix(fvScalarMatrix& matrix, word name)
{
    Info<< nl << name << ":" << nl << "##################" << nl;
    List<List<scalar> > mat(matrix.diag().size(), List<scalar>(matrix.diag().size(), 0.0));
    
    for(label k=0; k<matrix.diag().size(); k++) mat[k][k] = matrix.diag()[k];
    for(label k=0; k<matrix.lduAddr().lowerAddr().size(); k++)
    {
        label i = matrix.lduAddr().lowerAddr()[k];
        label j = matrix.lduAddr().upperAddr()[k];
        mat[i][j] = matrix.upper()[k];
        mat[j][i] = matrix.lower()[k];
    }
    
    for(label row=0; row<mat.size(); row++)
    {
        forAll(mat[row], item)
        {
            Info << setw(10) << mat[row][item] << "\t";
        }
        Info << "\tx_" << row << "\t=\t" << matrix.source()[row] << nl;
    }
}
