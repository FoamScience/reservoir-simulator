

    //// Store old values and update BCs
    //Un.correctBoundaryConditions();
    //Uw.correctBoundaryConditions();
    //// Set refs for phi if U is a fixedValue
    //forAll(mesh.boundary(),patchi)
    //{
    //    if (isA< fixedValueFvPatchField<vector> >(Un.boundaryField()[patchi]))
    //    {
    //        phin.boundaryField()[patchi] = Un.boundaryField()[patchi] & mesh.Sf().boundaryField()[patchi];
    //    }
    //    if (isA< fixedValueFvPatchField<vector> >(Uw.boundaryField()[patchi]))
    //    {
    //        phiw.boundaryField()[patchi] = Uw.boundaryField()[patchi] & mesh.Sf().boundaryField()[patchi];
    //    }
    //}
    fvScalarMatrix SwEqn
        (
            fvm::ddt(wStorage, phasew.alpha()) 
            + fvc::div(phiwG) 
            + fvc::div(phiPc) 
            // Explicit Wells contribution 
            - SrcwInj*phasew.iFVF() + SrcwProd*phasew.iFVF()
            + fvc::laplacian(-Mwf, p)
            // Implicit Wells contribution
            //+ (ImpwProd*phasew.iFVF()-ImpwInj*phasew.iFVF())*p
        );

    SwEqn.solve();

    Rw = fvc::ddt(wStorage, phasew.alpha())
            + fvc::div(phiwG) 
            + fvc::div(phiPc) 
            // Explicit Wells contribution 
            - SrcwInj*phasew.iFVF() + SrcwProd*phasew.iFVF()
            + fvc::laplacian(-Mwf, p)
            // Implicit Wells contribution
            //+ (ImpwProd*phasew.iFVF()-ImpwInj*phasew.iFVF())*p
         ;
    //debugMatrix(SwEqn, "SwEqn");

    fvScalarMatrix pInSw
        (
            fvm::laplacian(-Mwf, p)
            // Implicit Wells contribution
            + fvm::Sp( ImpwProd*phasew.iFVF()-ImpwInj*phasew.iFVF(), p)
         );


    //debugMatrix(pInSw, "pInSw");

    //// Populate block matrix
    //forAll(upper, nei)
    //{
    //    upper[nei][0] = SwEqn.upper()[nei];
    //    upper[nei][2] = pInSw.upper()[nei];
    //    lower[nei][0] = SwEqn.lower()[nei];
    //    lower[nei][2] = pInSw.lower()[nei];
    //}
    //forAll(diag, celli)
    //{
    //    diag[celli][0] = SwEqn.diag()[celli];
    //    source[celli][0] = SwEqn.source()[celli] + pInSw.source()[celli];
    //}
    
    //phasew.alpha().relax();

    //resEqn.insertEquation(0, SwEqn);
    //resEqn.insertEquationCoupling(0,1, pInSw);

