    fvScalarMatrix pEqn
        (
            fvm::ddt(pTimeCoeff, p) 
            + fvc::laplacian(-Mf, p) 
            + fvc::div(phiG)
            // Capillary flux
            + fvc::div(phiPc)
            // Explicit Wells contribution from 
            + SrcnProd*phasen.iFVF() + (SrcwProd-SrcwInj)*phasew.iFVF()
            // Implicit Wells contribution 
            //+ ( ImpnProd*phasen.iFVF()+(ImpwProd -ImpwInj)*phasew.iFVF())*p
        );

    BlockSolverPerformance<scalar> dd(pEqn.solve());

    // Recalculate residual, that's just stupid
    Rwn = fvc::ddt(pTimeCoeff, p)
            + fvc::laplacian(-Mf, p) 
            + fvc::div(phiG)
            // Capillary flux
            + fvc::div(phiPc)
            // Explicit Wells contribution from 
            + SrcnProd*phasen.iFVF() + (SrcwProd-SrcwInj)*phasew.iFVF()
            // Implicit Wells contribution 
            //+ ( ImpnProd*phasen.iFVF()+(ImpwProd -ImpwInj)*phasew.iFVF())*p
        ;
    Info << dd.finalResidual() << tab << gMin(Rwn) << tab << gMax(Rwn) << endl;
    //pEqn.elax();

    //debugMatrix(pEqn, "pEqn");

    //// Populate block matrix
    //forAll(upper, nei)
    //{
    //    upper[nei][3] = pEqn.upper()[nei];
    //    lower[nei][3] = pEqn.lower()[nei];
    //}
    //forAll(diag, celli)
    //{
    //    diag[celli][3] = pEqn.diag()[celli];
    //    source[celli][1] = pEqn.source()[celli];
    //}
    //
    //debugMatrix(pEqn, "pEqn");
     
    //pEqn.setReference(pRefCell, pRefValue);


    fvScalarMatrix SwInp
        (
            fvm::ddt(tStorage, phasew.alpha())
         );


    //resEqn.insertEquation(1, pEqn);
    //resEqn.insertEquationCoupling(1,0, SwInp);

