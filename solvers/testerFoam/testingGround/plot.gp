set term wxt persist

# Linear relationship
B(x)=1.051558-(7.5627e-10)*x

# Theor. derivative
diBdx(x) = (7.5627e-10)*1/B(x)**2

set title "d(1/B)/dP"
set xlabel "Pressure [Pa]"
set ylabel "d(1/B)/dP [Pa^{-1}]"
set format y "%.2e"

plot [300*6869:8000*6869] diBdx(x) w l lw 3 t 'Theory', 'Bw.data' every 40 u 1:2 w p t 'FVFModel'

set term pdfcairo 
set out "plot.pdf"
replot

